

var apn = require('apn');	
var path = require('path');


module.exports = function(config, tokens, payload) {
	var iOSHandler = function () { };

	function createOptions() {
		
		var options = {};
		options.pfx  =  config.pfx;
	    options.passphrase = config.passphrase;
        options.production = config.production;
	   return options;
	}
	
	function createConnection(options,  cb) {

		options.errorCallback = function(errCode, notification) {
            cb(errCode);
		};

		var service = new apn.connection(options);

		service.on('connected', function() {
		    console.log("Connected");
		});

		service.on('transmitted', function(notification, device) {
		    console.log("Notification transmitted to:" + device.token.toString('hex'));
            cb();
		});

		service.on('transmissionError', function(errCode, notification, device) {
		    console.error("Notification caused error: " + errCode + " for device ", device, notification);
		});

		service.on('timeout', function () {
		    console.log("Connection Timeout");
            cb();
		});
		

		service.on('socketError', function(err) {
			//There is a bug here in the module. if you are not connected, this event keeps getting called endless until a conenction is back.
			//console.log("socketError" + err);
			
		});
		return service;
	}
	
	iOSHandler.prototype = {
			sendMsg : function(callback) {

                var deviceTokens = tokens;

                if (!deviceTokens || deviceTokens.length <= 0) {
                    callback( "empty tokens");
                    return;
                }


                var note = new apn.Notification();
                note.payload = payload;
                note.alert = payload.aps.alert;
                note.sound = payload.aps.sound;
                note.badge = payload.aps.badge;


                var options = createOptions();
                var service = createConnection(options, callback);

                service.pushNotification(note,deviceTokens);

            }
	};
	return new iOSHandler();
};